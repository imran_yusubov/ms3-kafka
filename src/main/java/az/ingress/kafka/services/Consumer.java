package az.ingress.kafka.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Consumer {

    @KafkaListener(topics = "baeldung", groupId = "ingress1")
    public void listenGroupFoo(String message) throws InterruptedException {
        log.info("Received Message in group ingress1 {} ", message);
        Thread.sleep((long) (Math.random() * 10000));
    }

    @KafkaListener(topics = "baeldung", groupId = "ingress1")
    public void listenGroupFoo2(String message) throws InterruptedException {
        log.info("Received Message in group ingress2 {} ", message);
        Thread.sleep((long) (Math.random() * 10000));
    }
}
